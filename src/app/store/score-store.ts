import { create } from 'zustand';

type ScoreStoreState = {
	wins: number;
	loses: number;
	addWin: () => void;
	addLose: () => void;
};

export const useScoreStore = create<ScoreStoreState>((set) => ({
	wins: 0,
	loses: 0,
	addWin: () => set((state) => ({ ...state, wins: state.wins + 1 })),
	addLose: () => set((state) => ({ ...state, loses: state.loses + 1 })),
}));
