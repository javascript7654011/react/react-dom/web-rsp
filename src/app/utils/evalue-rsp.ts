import { Element, Result } from '@/models';

export function evalueRsp(userElement: Element, computerElement: Element) {
	if (userElement === computerElement) return Result.Draw;
	if (userElement === Element.Rock)
		return computerElement === Element.Paper ? Result.Lose : Result.Win;
	if (userElement === Element.Paper)
		return computerElement === Element.Scissors ? Result.Lose : Result.Win;
	return computerElement === Element.Rock ? Result.Lose : Result.Win;
}
