import { Element, Result } from '@/models';
import { StartGame } from '@/ui/templates/start-game';
import { useState } from 'react';
import { ComputerSelect } from './ui/templates/computer-select';
import { ResultGame } from './ui/templates/result-game';

export function App() {
	const [stepper, setStepper] = useState(0);
	const [userElement, setUserElement] = useState<Element | null>(null);
	const [computerElement, setComputerElement] = useState<Element | null>(null);
	const [result, setResult] = useState<Result | null>(null);

	const firstStepper = (element: Element) => {
		setUserElement(element);
		setStepper(1);
	};

	const secondStepper = (element: Element, result: Result) => {
		setComputerElement(element);
		setResult(result);
		setStepper(2);
	};

	const resetStepper = () => {
		setStepper(0);
		setUserElement(null);
		setComputerElement(null);
	};

	return (
		<div className="flex flex-col items-center h-full w-screen">
			{stepper === 0 && <StartGame selectUserElement={firstStepper} />}
			{stepper === 1 && (
				<ComputerSelect
					userElement={userElement as Element}
					nextStep={secondStepper}
				/>
			)}
			{stepper === 2 && (
				<ResultGame
					resultGame={result as Result}
					resetGame={resetStepper}
					userElement={userElement as Element}
					computerElement={computerElement as Element}
				/>
			)}
		</div>
	);
}

export default App;
