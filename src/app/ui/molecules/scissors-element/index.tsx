import { CircleElement } from '@/ui/atoms/circle-element';
import { SvgScissors } from '@/ui/atoms/svgs';

type ScissorsElementProps = React.HTMLAttributes<HTMLDivElement> & {
	width?: number;
	height?: number;
};

export function ScissorsElement({
	width = 120,
	height = 120,
	...props
}: ScissorsElementProps) {
	return (
		<CircleElement
			className={`bg-scissors-start border-scissors-end ${
				props?.className ?? ''
			}`}
			width={width}
			height={height}
		>
			<SvgScissors />
		</CircleElement>
	);
}
