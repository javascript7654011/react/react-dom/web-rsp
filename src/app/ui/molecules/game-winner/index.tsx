import { Result } from '@/models';
import { BtnContained } from '@/ui/atoms/buttons';
import { Typography } from '@/ui/atoms/typography';

type GameWinnerProps = {
	resultGame: Result;
	resetGame: () => void;
};

export function GameWinner({ resetGame, resultGame }: GameWinnerProps) {
	const getResult = () => {
		if (resultGame === Result.Draw) return 'YOU DRAW';
		if (resultGame === Result.Win) return 'YOU WIN';
		return 'YOU LOSE';
	};

	return (
		<div className="flex flex-col items-center justify-center h-full">
			<Typography
				variant="h1"
				color="primary"
				weight="bold"
				elementProps={{
					style: { fontSize: 60, marginBottom: 40 },
					className: 'drop-shadow whitespace-nowrap',
				}}
			>
				{getResult()}
			</Typography>
			<BtnContained
				className="uppercase text-lg tracking-widest"
				onClick={resetGame}
			>
				PLAY AGAIN
			</BtnContained>
		</div>
	);
}
