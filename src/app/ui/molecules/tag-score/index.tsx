import { useScoreStore } from '@/store';
import { Tag } from '@/ui/atoms/tag';
import { Typography } from '@/ui/atoms/typography';

type TypographyElement = {
	variant: 'h2' | 'p' | 'h1' | 'span';
	weight: 'normal' | 'bold';
	color: 'primary' | 'secondary' | 'tertiary';
	spacing: 'normal' | 'extended';
	fontSize: number;
	content: string;
};

export function TagScore() {
	const [wins, loses] = useScoreStore((state) => [state.wins, state.loses]);
	const typographies: TypographyElement[] = [
		{
			variant: 'h2',
			weight: 'bold',
			color: 'secondary',
			spacing: 'extended',
			fontSize: 14,
			content: 'Score',
		},
		{
			variant: 'p',
			weight: 'bold',
			color: 'tertiary',
			spacing: 'normal',
			fontSize: 40,
			content: String(wins - loses),
		},
	];

	return (
		<Tag className="flex flex-col items-center rounded-lg justify-center m-4 sm:m-0">
			{typographies.map((typography, index) => (
				<Typography
					key={index}
					variant={typography.variant}
					weight={typography.weight}
					color={typography.color}
					spacing={typography.spacing}
					elementProps={{
						style: { fontSize: typography.fontSize, lineHeight: 1 },
					}}
				>
					{typography.content}
				</Typography>
			))}
		</Tag>
	);
}
