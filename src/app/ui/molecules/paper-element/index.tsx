import { CircleElement } from '@/ui/atoms/circle-element';
import { SvgPaper } from '@/ui/atoms/svgs';

type PaperElementProps = React.HTMLAttributes<HTMLDivElement> & {
	width?: number;
	height?: number;
};

export function PaperElement({
	width = 120,
	height = 120,
	...props
}: PaperElementProps) {
	return (
		<CircleElement
			className={`bg-paper-start border-paper-end ${props?.className ?? ''}`}
			width={width}
			height={height}
		>
			<SvgPaper />
		</CircleElement>
	);
}
