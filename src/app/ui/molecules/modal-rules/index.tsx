import { BtnNoBorder } from '@/ui/atoms/buttons';
import { Modal } from '@/ui/atoms/modal';
import { SvgClose, SvgRules } from '@/ui/atoms/svgs';
import { Typography } from '@/ui/atoms/typography';

type ModalRulesProps = {
	show: boolean;
	hide: () => void;
};

export function ModalRules({ show, hide }: ModalRulesProps) {
	return (
		<Modal show={show}>
			<div className="bg-white relative select-none rounded-lg py-4 px-8">
				<BtnNoBorder className="absolute top-2 right-0 scale-75" onClick={hide}>
					<SvgClose />
				</BtnNoBorder>
				<Typography
					variant="h2"
					color="tertiary"
					weight="bold"
					elementProps={{
						style: { marginTop: '8px', marginBottom: '18px', fontSize: 24 },
					}}
				>
					Rules
				</Typography>
				<SvgRules />
			</div>
		</Modal>
	);
}
