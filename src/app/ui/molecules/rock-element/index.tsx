import { CircleElement } from '@/ui/atoms/circle-element';
import { SvgRock } from '@/ui/atoms/svgs';

type RockElementProps = React.HTMLAttributes<HTMLDivElement> & {
	width?: number;
	height?: number;
};

export function RockElement({
	width = 120,
	height = 120,
	...props
}: RockElementProps) {
	return (
		<CircleElement
			{...props}
			className={`bg-rock-start border-rock-end ${props?.className ?? ''}`}
			width={width}
			height={height}
		>
			<SvgRock />
		</CircleElement>
	);
}
