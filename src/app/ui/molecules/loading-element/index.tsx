import { CircleElement } from '@/ui/atoms/circle-element';

export function LoadingElement() {
	return (
		<CircleElement
			className="bg-neutral-800 border-neutral-800"
			width={120}
			height={120}
		></CircleElement>
	);
}
