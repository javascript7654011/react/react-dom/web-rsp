import { BtnOutline } from '@/ui/atoms/buttons';
import { Typography } from '@/ui/atoms/typography';

type BtnRulesProps = React.ButtonHTMLAttributes<HTMLButtonElement>;

export function BtnRules(props: BtnRulesProps) {
	return (
		<BtnOutline {...props} className={`select-none ${props?.className ?? ''}`}>
			<Typography variant="span">Rules</Typography>
		</BtnOutline>
	);
}
