import { SvgProps } from '@/models';

export function SvgClose({
	color = '#3B4262',
	width = '20',
	height = '20',
}: SvgProps) {
	return (
		<svg width={width} height={height}>
			<path
				fill={color}
				fillRule="evenodd"
				d="M16.97 0l2.122 2.121-7.425 7.425 7.425 7.425-2.121 2.12-7.425-7.424-7.425 7.425L0 16.97l7.425-7.425L0 2.121 2.121 0l7.425 7.425L16.971 0z"
			/>
		</svg>
	);
}
