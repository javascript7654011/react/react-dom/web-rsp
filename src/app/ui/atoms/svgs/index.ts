export * from './svg-close';
export * from './svg-logo-rsp';
export * from './svg-paper';
export * from './svg-rock';
export * from './svg-rules';
export * from './svg-scissors';
export * from './svg-triangule';
