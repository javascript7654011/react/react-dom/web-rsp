import { SvgProps } from '@/models';

export function SvgTriangule({
	color = '#3B4262',
	width = '313',
	height = '278',
}: SvgProps) {
	return (
		<svg width={width} height={height}>
			<path
				stroke={color}
				strokeWidth="15"
				fill="none"
				d="M156.5 262 300 8H13z"
			/>
		</svg>
	);
}
