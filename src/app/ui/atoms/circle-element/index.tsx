import React from 'react';

type CircleElementProps = React.HTMLAttributes<HTMLDivElement> & {
	children?: React.ReactNode;
	width: number;
	height: number;
	sizeBorderContainer?: number;
	sizeBorderChildren?: number;
};

export function CircleElement({
	children,
	width,
	height,
	sizeBorderChildren = 6,
	sizeBorderContainer = 8,
	...divProps
}: CircleElementProps) {
	return (
		<div
			{...divProps}
			className={`p-[18px] rounded-full h-fit w-fit ${divProps.className}`}
			style={{
				borderBottomWidth: sizeBorderContainer,
			}}
		>
			<div
				className={`${
					children ? 'bg-white border-neutral-400' : 'bg-neutral-800'
				} flex items-center justify-center rounded-full`}
				style={{
					width,
					height,
					borderTopWidth: children ? sizeBorderChildren : 0,
				}}
			>
				{children}
			</div>
		</div>
	);
}
