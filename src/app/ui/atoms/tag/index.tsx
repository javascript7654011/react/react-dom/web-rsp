type TagProps = React.HTMLAttributes<HTMLDivElement> & {
	children: React.ReactNode;
};

export function Tag({ children, ...divProps }: TagProps) {
	return (
		<div
			{...divProps}
			className={`bg-white py-4 px-8 shadow-lg ${divProps?.className ?? ''}`}
		>
			{children}
		</div>
	);
}
