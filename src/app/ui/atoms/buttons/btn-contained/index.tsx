type BtnContainedProps = React.ButtonHTMLAttributes<HTMLButtonElement> & {
	children: React.ReactNode;
};

export function BtnContained({ children, ...btnProps }: BtnContainedProps) {
	return (
		<button
			type="button"
			{...btnProps}
			className={`
        bg-white shadow text-dark-text hover:text-rock-start rounded-lg 
        py-[12px] px-16 h-fit w-fit ${btnProps?.className ?? ''}
      `}
		>
			{children}
		</button>
	);
}
