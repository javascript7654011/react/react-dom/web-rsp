type BtnOutlineProps = React.ButtonHTMLAttributes<HTMLButtonElement> & {
	children: React.ReactNode;
};

export function BtnOutline({ children, ...btnProps }: BtnOutlineProps) {
	return (
		<button
			type="button"
			{...btnProps}
			className={`
        bg-transparent hover:opacity-80 rounded-lg outline outline-2 outline-white py-2 px-16 h-fit w-fit
          ${btnProps?.className ?? ''}
      `}
		>
			{children}
		</button>
	);
}
