type BtnNoBorderProps = React.ButtonHTMLAttributes<HTMLButtonElement> & {
	children: React.ReactNode;
};

export function BtnNoBorder({ children, ...btnProps }: BtnNoBorderProps) {
	return (
		<button
			type="button"
			{...btnProps}
			className={`rounded px-4 py-2 hover:opacity-80 ${btnProps?.className}`}
		>
			{children}
		</button>
	);
}
