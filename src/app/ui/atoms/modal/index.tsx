type ModalProps = {
	show: boolean;
	children: React.ReactNode;
};

export function Modal({ show, children }: ModalProps) {
	return (
		<div
			className={`fixed bg-black bg-opacity-30 top-0 left-0 z-50 ${
				show ? 'flex' : 'hidden'
			} items-center justify-center w-screen h-screen`}
		>
			{children}
		</div>
	);
}
