type TypographyProps = {
	variant?: 'h1' | 'h2' | 'p' | 'span';
	elementProps?: React.HTMLProps<HTMLHeadingElement>;
	color?: 'primary' | 'secondary' | 'tertiary';
	size?:
		| 'sm'
		| 'md'
		| 'lg'
		| 'xl'
		| '2xl'
		| '3xl'
		| '4xl'
		| '5xl'
		| '6xl'
		| '7xl'
		| '8xl'
		| '9xl'
		| '10xl';
	weight?: 'normal' | 'bold';
	spacing?: 'normal' | 'extended';
	position?: 'start' | 'center' | 'end';
	children: React.ReactNode;
};

export function Typography({
	variant = 'h1',
	children,
	elementProps,
	color = 'primary',
	size = 'lg',
	weight,
	spacing = 'normal',
	position = 'start',
}: TypographyProps) {
	const Element = variant;

	const getColor = (): string => {
		if (color === 'primary') return 'text-white';
		else if (color === 'secondary') return 'text-score-text';
		return 'text-dark-text';
	};

	const getSize = (): string => `text-${size}`;

	const getFontWeight = (): number => {
		if (weight === 'bold') return 700;
		return 600;
	};

	const getSpacing = (): string => {
		if (spacing === 'extended') return 'tracking-wider';
		return 'tracking-normal';
	};

	const getPosition = (): string => `text-${position}`;

	return (
		<Element
			{...elementProps}
			className={`${getColor()} ${getSpacing()} ${getSize()} uppercase ${
				elementProps?.className ?? ''
			} ${getPosition()}`}
			style={{ fontWeight: getFontWeight(), ...(elementProps?.style ?? {}) }}
		>
			{children}
		</Element>
	);
}
