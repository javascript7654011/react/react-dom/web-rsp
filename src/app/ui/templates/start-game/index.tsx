import { Element } from '@/models';
import { GameScore } from '@/ui/organisms/game-score';
import { SelectElements } from '@/ui/organisms/select-elements';
import { ViewRules } from '@/ui/organisms/view-rules';

type StartGameProps = {
	selectUserElement: (element: Element) => void;
};

export function StartGame({ selectUserElement }: StartGameProps) {
	return (
		<div className="flex flex-col items-center py-8 px-16 justify-between min-h-screen w-full max-w-screen">
			<GameScore />
			<SelectElements
				selectElement={selectUserElement}
				style={{ marginTop: 120 }}
			/>
			<div className="flex items-center justify-center sm:justify-end w-screen">
				<ViewRules className="mt-16" />
			</div>
		</div>
	);
}
