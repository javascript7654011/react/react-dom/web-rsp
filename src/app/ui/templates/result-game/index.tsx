import { Element, Result } from '@/models';
import { GameWinner } from '@/ui/molecules/game-winner';
import { ElementSelected } from '@/ui/organisms/element-selected';
import { GameScore } from '@/ui/organisms/game-score';
import { ViewRules } from '@/ui/organisms/view-rules';

type ResultGameProps = {
	userElement: Element;
	computerElement: Element;
	resultGame: Result;
	resetGame: () => void;
};

export function ResultGame({
	userElement,
	computerElement,
	resetGame,
	resultGame,
}: ResultGameProps) {
	return (
		<div className="flex flex-col items-center py-8 px-16 justify-between min-h-screen w-full max-w-screen">
			<GameScore />
			<div
				className="flex flex-col md:flex-row items-center justify-evenly w-full"
				style={{ marginTop: 120 }}
			>
				<ElementSelected
					element={userElement}
					typhographyProps={{ className: 'left-[10px]' }}
					title="You picked"
					className="mb-28 md:mb-0"
				/>
				<GameWinner resultGame={resultGame} resetGame={resetGame} />
				<ElementSelected
					element={computerElement}
					typhographyProps={{ className: 'left-[-15px]' }}
					title="The house picked"
					className="mt-40 md:mt-0"
				/>
			</div>
			<div className="flex items-center justify-center sm:justify-end px-8 w-screen">
				<ViewRules className="mt-16" />
			</div>
		</div>
	);
}
