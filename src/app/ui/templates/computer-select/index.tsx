import { useComputedSelectHook } from '@/hooks/computer-select.hook';
import { Element, Result } from '@/models';
import { useScoreStore } from '@/store';
import { ElementSelected } from '@/ui/organisms/element-selected';
import { GameScore } from '@/ui/organisms/game-score';
import { ViewRules } from '@/ui/organisms/view-rules';
import { evalueRsp } from '@/utils/evalue-rsp';
import { useEffect } from 'react';

type ComputerSelectProps = {
	userElement: Element;
	nextStep: (element: Element, result: Result) => void;
};

export function ComputerSelect({ userElement, nextStep }: ComputerSelectProps) {
	const [computerElement] = useComputedSelectHook();
	const [addWin, addLose] = useScoreStore((state) => [
		state.addWin,
		state.addLose,
	]);

	const setResultStore = (result: Result) => {
		if (result === Result.Win) {
			addWin();
		} else if (result === Result.Lose) {
			addLose();
		}
	};

	useEffect(() => {
		if (computerElement) {
			setTimeout(() => {
				const result = evalueRsp(userElement, computerElement);
				nextStep(computerElement, result);
				setResultStore(result);
			}, 500);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [computerElement]);

	return (
		<div className="flex flex-col items-center py-8 px-16 justify-between min-h-screen w-full max-w-screen">
			<GameScore />

			<div
				className="flex flex-col sm:flex-row items-center justify-evenly w-full"
				style={{ marginTop: 120 }}
			>
				<ElementSelected
					element={userElement}
					title="You picked"
					typhographyProps={{ className: 'left-[10px]' }}
					className="mb-20 sm:mb-0"
				/>
				<ElementSelected
					element={computerElement}
					title="The house picked"
					typhographyProps={{ className: 'left-[-15px]' }}
					className="mt-20 sm:mt-0"
				/>
			</div>
			<div className="flex items-center justify-center sm:justify-end w-screen">
				<ViewRules className="mt-16" />
			</div>
		</div>
	);
}
