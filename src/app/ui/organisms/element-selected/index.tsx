import { Element } from '@/models';
import { Typography } from '@/ui/atoms/typography';
import { LoadingElement } from '@/ui/molecules/loading-element';
import { PaperElement } from '@/ui/molecules/paper-element';
import { RockElement } from '@/ui/molecules/rock-element';
import { ScissorsElement } from '@/ui/molecules/scissors-element';

type ElementSelectedProps = React.DetailedHTMLProps<
	React.HTMLAttributes<HTMLDivElement>,
	HTMLDivElement
> & {
	element: Element | null;
	title: string;
	typhographyProps?: React.HTMLProps<HTMLHeadingElement>;
};

export function ElementSelected({
	element,
	title,
	typhographyProps,
	...props
}: ElementSelectedProps) {
	return (
		<div
			{...props}
			className={`flex flex-col items-center relative w-fit h-fit ${
				props?.className ?? ''
			}`}
		>
			<Typography
				variant="h2"
				color="primary"
				weight="bold"
				spacing="extended"
				position="center"
				elementProps={{
					className: `absolute top-[-80px] left-0 whitespace-nowrap ${
						typhographyProps?.className ?? ''
					}`,
					style: { fontSize: 24 },
				}}
			>
				{title}
			</Typography>
			{element === Element.Rock && <RockElement className="scale-125" />}
			{element === Element.Paper && <PaperElement className="scale-125" />}
			{element === Element.Scissors && (
				<ScissorsElement className="scale-125" />
			)}
			{!element && <LoadingElement />}
		</div>
	);
}
