import { BtnRules } from '@/ui/molecules/btn-rules';
import { ModalRules } from '@/ui/molecules/modal-rules';
import { useState } from 'react';

type ViewRulesProps = React.DetailedHTMLProps<
	React.HTMLAttributes<HTMLDivElement>,
	HTMLDivElement
>;

export function ViewRules({ ...divProps }: ViewRulesProps) {
	const [show, setShow] = useState(false);

	const showModal = () => setShow(true);
	const hideModal = () => setShow(false);

	return (
		<div
			{...divProps}
			className={`m-4 h-fit w-min  ${divProps?.className ?? ''}`}
		>
			<BtnRules onClick={showModal} />
			<ModalRules show={show} hide={hideModal} />
		</div>
	);
}
