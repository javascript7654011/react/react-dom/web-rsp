import { Element } from '@/models';
import { SvgTriangule } from '@/ui/atoms/svgs';
import { PaperElement } from '@/ui/molecules/paper-element';
import { RockElement } from '@/ui/molecules/rock-element';
import { ScissorsElement } from '@/ui/molecules/scissors-element';

type SelectElementsProps = React.DetailedHTMLProps<
	React.HTMLAttributes<HTMLDivElement>,
	HTMLDivElement
> & {
	selectElement: (element: Element) => void;
};

export function SelectElements({
	selectElement,
	...props
}: SelectElementsProps) {
	const selectRock = () => selectElement(Element.Rock);
	const selectPaper = () => selectElement(Element.Paper);
	const selectScissors = () => selectElement(Element.Scissors);

	return (
		<div
			{...props}
			className={`relative w-fit h-fit ${props?.className ?? ''}`}
		>
			<button
				type="button"
				className="absolute top-[-75px] left-[-30px]"
				onClick={selectPaper}
			>
				<PaperElement />
			</button>
			<button
				type="button"
				className="absolute top-[-75px] right-[-35px]"
				onClick={selectScissors}
			>
				<ScissorsElement />
			</button>
			<button
				type="button"
				className="absolute bottom-[-25px] left-[25%]"
				onClick={selectRock}
			>
				<RockElement />
			</button>
			<SvgTriangule color="#00000060" />
		</div>
	);
}
