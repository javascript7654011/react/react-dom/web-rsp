import { SvgLogoRsp } from '@/ui/atoms/svgs';
import { TagScore } from '@/ui/molecules/tag-score';

type GameScoreProps = React.DetailedHTMLProps<
	React.HTMLAttributes<HTMLDivElement>,
	HTMLDivElement
>;

export function GameScore(props: GameScoreProps) {
	return (
		<div
			{...props}
			className={`flex flex-wrap items-center justify-center sm:justify-between rounded-lg p-4 outline outline-2 outline-white min-w-fit max-w-[800px] w-[80%] ${
				props?.className ?? ''
			}`}
		>
			<SvgLogoRsp />
			<TagScore />
		</div>
	);
}
