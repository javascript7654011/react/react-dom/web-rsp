import { Element } from '@/models';
import { useEffect, useState } from 'react';

export function useComputedSelectHook() {
	const [computer, setComputer] = useState<Element | null>(null);

	useEffect(() => {
		setTimeout(() => {
			const elements = [Element.Rock, Element.Paper, Element.Scissors];
			const randomElement =
				elements[Math.floor(Math.random() * elements.length)];
			setComputer(randomElement);
		}, 500);
	}, []);

	return [computer];
}
