export enum Result {
	Win = 'win',
	Lose = 'lose',
	Draw = 'draw',
}
