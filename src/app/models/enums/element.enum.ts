export enum Element {
	Rock = 'rock',
	Paper = 'paper',
	Scissors = 'scissors',
}
