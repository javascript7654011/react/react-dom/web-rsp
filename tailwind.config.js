const { createGlobPatternsForDependencies } = require('@nx/react/tailwind');
const { join } = require('path');

/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		join(
			__dirname,
			'{src,pages,components,app}/**/*!(*.stories|*.spec).{ts,tsx,html}'
		),
		...createGlobPatternsForDependencies(__dirname),
	],
	theme: {
		extend: {
			colors: {
				'scissors-start': '#ec9e0e',
				'scissors-end': '#b88012',
				'paper-start': '#4865f4',
				'paper-end': '#1f3cce',
				'rock-start': '#dc2e4e',
				'rock-end': '#9b0924',
				'lizard-start': '#834fe3',
				'lizard-end': '#8c5de5',
				'cyan-start': '#40b9ce',
				'cyan-end': '#52bed1',
				'dark-text': '#3b4363',
				'score-text': '#2a46c0',
				'header-outline': '#606e85',
			},
		},
	},
	plugins: [],
};
